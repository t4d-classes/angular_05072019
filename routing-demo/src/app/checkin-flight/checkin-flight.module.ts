import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckinFlightRouterModule } from './checkin-flight.routing';

import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    CheckinFlightRouterModule,
  ]
})
export class CheckinFlightModule { }
