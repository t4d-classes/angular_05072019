import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookFlightRouterModule } from './book-flight.routing';

import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    BookFlightRouterModule,
  ]
})
export class BookFlightModule { }
