import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomeComponent },
  {
    path: 'book',
    loadChildren: './book-flight/book-flight.module#BookFlightModule'
  },
  {
    path: 'checkin',
    loadChildren: './checkin-flight/checkin-flight.module#CheckinFlightModule'
  },
];

export const AppRouterModule = RouterModule.forRoot(routes);