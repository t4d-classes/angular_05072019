import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// import { Observable, Observer, of, concat, merge, interval } from 'rxjs';
// import { take, map, filter } from 'rxjs/operators';

// const nums = Observable.create((observer: Observer<number>) => {

//   console.log('called observable function');

//   let counter = 0;

//   const h = setInterval(() => {

//     console.log('closed: ', observer.closed);

//     if (observer.closed) {
//       clearInterval(h);
//       return;
//     }

//     observer.next(counter++);

//   }, 500);

  // setTimeout(() => {
  //   clearInterval(h);
  //   observer.complete();
  // }, 4000);

// });

// nums.pipe(
//   take(10), filter(n => n > 5), map(n => n * 2)
// ).subscribe(num => {
//   console.log(num);
// }, null, () => {
//   console.log('all done');
// });

// const nums = of(1, 2, 3, 4, 5);

// const letters = of('a', 'b', 'c', 'd', 'e');


// merge(
//   interval(500),
//   interval(1000).pipe(map(n => n * 1000))
// ).subscribe(f => console.log(f));
