import { MyUppercasePipe } from './my-uppercase.pipe';

describe('MyUppercasePipe', () => {

  it('create an instance', () => {
    const pipe = new MyUppercasePipe();
    expect(pipe).toBeTruthy();
  });

  it('should me make text uppercase', () => {
    const pipe = new MyUppercasePipe();
    expect(pipe.transform('hi')).toEqual('HI');
  });

});
