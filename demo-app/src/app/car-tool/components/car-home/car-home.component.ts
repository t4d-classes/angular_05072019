import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { CarsService } from '../../services/cars.service';

import { Car } from '../../models/car';

@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {

  cars: Car[] = [];

  editCarId = -1;

  constructor(private carsSvc: CarsService) { }

  ngOnInit() {
    this.carsSvc.all().pipe(
      map(cars => cars.map(car => {
        car.price = car.price * 1000;
        return car;
      }))
    ).subscribe(cars => this.cars = cars);
  }

  doAddCar(car: Car) {
    this.cars = this.cars.concat({
      ...car,
      id: Math.max(...this.cars.map(c => c.id), 0) + 1,
    });
    this.editCarId = -1;
  }

  doReplaceCar(car: Car) {
    const newCars = this.cars.concat();
    const carIndex = newCars.findIndex(c => c.id === car.id);
    newCars[carIndex] = car;
    this.cars = newCars;
    this.editCarId = -1;
  }

  doDeleteCar(carId: number) {
    this.cars = this.cars.filter(c => c.id !== carId);
    this.editCarId = -1;
  }

  doEditCar(carId: number) {
    this.editCarId = carId;
  }

  doCancelCar() {
    this.editCarId = -1;
  }


}
