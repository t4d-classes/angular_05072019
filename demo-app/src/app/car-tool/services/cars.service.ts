import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Car } from '../models/car';

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  constructor(private httpClient: HttpClient) { }

  all() {
    return this.httpClient
      .get<Car[]>('http://localhost:4250/cars');
  }

  append(car: Car) {

    // REST: https://en.wikipedia.org/wiki/Representational_state_transfer
    // JSON Server: https://github.com/typicode/json-server

    // Lab Exercise: 

    // 1. Implement append, replace and delete REST service operations using HttpClient and Promises

    // 2. After performing an append, replace, or delete, refresh the table cars from the REST service

    // 3. Add a button above the table with the label of Refresh. When the button is clicked refresh
    // the cars in the table from the REST service

    return this.httpClient
      .post<Car>('http://localhost:4250/cars', car);

  }
}
