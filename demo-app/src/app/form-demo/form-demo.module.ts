import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { NameInputComponent } from './components/name-input/name-input.component';
import { SampleFormArrComponent } from './components/sample-form-arr/sample-form-arr.component';
import { TypeaheadDemoComponent } from './components/typeahead-demo/typeahead-demo.component';

@NgModule({
  declarations: [ContactFormComponent, SampleFormArrComponent, NameInputComponent, TypeaheadDemoComponent],
  imports: [
    CommonModule, ReactiveFormsModule,
  ],
  exports: [ContactFormComponent, SampleFormArrComponent, NameInputComponent,  TypeaheadDemoComponent]
})
export class FormDemoModule { }
