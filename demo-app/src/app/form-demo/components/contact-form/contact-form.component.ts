import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { myRequiredValidator, emailOrPhoneValidator } from '../../../shared/validators/contact-validators'; 

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  // firstNameInput: FormControl;

  contactForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  get firstNameShowValidationMsg() {

    const firstNameInput = this.contactForm.get('firstName') as FormControl;

    return firstNameInput.invalid
      && firstNameInput.touched;
  }

  ngOnInit() {

    // this.firstNameInput = this.fb.control('');

    this.contactForm = this.fb.group({
      firstName: [ '', { validators: [ myRequiredValidator ] } ],
      email: [ '' ],
      phone: [ '' ],
      fullName: [ '' ],
    }, {
      validators: [ emailOrPhoneValidator ]
    });

    console.log(this.contactForm.get('firstName').invalid);
  }

}
