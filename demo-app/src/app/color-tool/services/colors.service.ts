import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ColorsService {

  private colors: string[] = [ 'green', 'blue', 'olive', 'crimson', 'purple' ];

  constructor() { }

  all() {
    return this.colors.concat();
  }

}