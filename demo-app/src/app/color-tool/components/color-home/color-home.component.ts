import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ColorsService } from '../../services/colors.service';

@Component({
  selector: 'color-home',
  templateUrl: './color-home.component.html',
  styleUrls: ['./color-home.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class ColorHomeComponent implements OnInit {

  headerText = 'Color Tool';

  colors: string[] = [];

  constructor(private colorsSvc: ColorsService ) { }

  ngOnInit() {
    this.colors = this.colorsSvc.all();
  }

  addColor(newColor: string) {
    this.colors = this.colors.concat(newColor);
  }

}

