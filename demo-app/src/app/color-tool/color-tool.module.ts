import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { ColorHomeComponent } from './components/color-home/color-home.component';
import { ColorFormComponent } from './components/color-form/color-form.component';
// import { ColorsService } from './services/colors.service';

@NgModule({
  declarations: [ColorHomeComponent, ColorFormComponent],
  exports: [ColorHomeComponent],
  imports: [
    CommonModule, ReactiveFormsModule, SharedModule,
  ],
  // providers: [ { provide: ColorsService, useClass: ColorsService } ],
  //providers: [ ColorsService ],
})
export class ColorToolModule { }
